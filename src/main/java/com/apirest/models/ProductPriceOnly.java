package com.apirest.models;

public class ProductPriceOnly {
    private long id;
    private double price;

    public ProductPriceOnly() {
    }

    public ProductPriceOnly(long id) {
        this.id = id;
    }

    public ProductPriceOnly(double price) {
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
