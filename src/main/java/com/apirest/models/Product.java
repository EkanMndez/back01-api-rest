package com.apirest.models;

import java.util.List;
import java.util.Objects;

public class Product {
    private long id;
    private String description;
    private double price;
    private List<User> users;

    public Product() {
    }

    public Product(long id, String description, double price) {
        this.id = id;
        this.description = description;
        this.price = price;
    }

    public Product(long id, String description, double price, List<User> users) {
        this.id = id;
        this.description = description;
        this.price = price;
        this.users = users;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        String sb = "Producto{" + "id=" + id +
                ", descripcion='" + description + '\'' +
                ", precio=" + price +
                ", users=" + users +
                '}';
        return sb;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getId() == product.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
