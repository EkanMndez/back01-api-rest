package com.apirest.services;

import com.apirest.models.Product;

import java.util.List;

public interface ProductService {
    List<Product> getProducts();

    Product getProduct(long id);

    Product addProduct(Product newProduct);

    Product updateProduct(long id, Product newProduct);

    void removeProduct(int id);

    int getIndex(long index);
}
