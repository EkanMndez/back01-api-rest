package com.apirest.services.impl;

import com.apirest.models.Product;
import com.apirest.models.User;
import com.apirest.services.ProductService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final List<Product> products;
    private final AtomicLong idSequence = new AtomicLong(5);

    public ProductServiceImpl() {
        products = new ArrayList<>();
        products.add(new Product(1, "Playera", 100.00));
        products.add(new Product(2, "Pantalon", 250.00));
        products.add(new Product(3, "Camisa", 325.99));
        products.add(new Product(4, "Gorra", 199.99));
        products.add(new Product(5, "Zapatos", 650.35));

        List<User> users = new ArrayList<>();
        users.add(new User("1"));
        users.add(new User("3"));
        users.add(new User("5"));

        products.get(1).setUsers(users);
    }

    @Override
    public List<Product> getProducts() {
        return products;
    }

    @Override
    public Product getProduct(long id) throws IndexOutOfBoundsException {
        return products.stream().filter(
                product -> product.getId() == id
        ).collect(Collectors.toList()).get(0);
    }

    @Override
    public Product addProduct(Product newProduct) {
        long id = idSequence.incrementAndGet();
        newProduct.setId(id);
        products.add(newProduct);
        return newProduct;
    }

    @Override
    public Product updateProduct(long id, Product newProduct) {
        final Product original = products.stream().filter(product -> product.getId() == id).collect(Collectors.toList()).get(0);
        if (original == null)
            throw new RuntimeException();
        final int index = products.indexOf(original);

        products.get(index).setDescription(newProduct.getDescription());
        products.get(index).setPrice(newProduct.getPrice());

        return products.get(index);
    }

    @Override
    public void removeProduct(int id) {
        final Product original = products.stream().filter(product -> product.getId() == id).collect(Collectors.toList()).get(0);
        if (original == null)
            throw new RuntimeException();

        products.remove(original);
    }

    @Override
    public int getIndex(long index) {
        return 0;
    }
}
