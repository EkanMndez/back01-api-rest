package com.apirest.controllers;

import com.apirest.models.Product;
import com.apirest.models.ProductPriceOnly;
import com.apirest.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("${url.base}")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("")
    public String root() {
        return "Techu API REST v1.0.0";
    }

    @GetMapping("/products")
    public List<Product> getProducts() {
        return productService.getProducts();
    }

    @GetMapping("/products/size")
    public int countProducts() {
        return productService.getProducts().size();
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable long id) {
        Product product;
        try {
            product = productService.getProduct(id);
        } catch (RuntimeException rte) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(product);
    }

    @PostMapping("/products")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Product> createProduct(@RequestBody Product product) {
        productService.addProduct(product);
        return ResponseEntity.ok(product);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable long id, @RequestBody Product productUpdated) {
        Product product;
        try {
            product = productService.getProduct(id);
        } catch (RuntimeException rte) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        product = productService.updateProduct(id, productUpdated);

        return ResponseEntity.ok(product);
    }

    @PatchMapping("/products/{id}")
    public ResponseEntity<Product> patchPriceProduct(@PathVariable long id, @RequestBody ProductPriceOnly productPriceOnly) {
        Product product;
        try {
            product = productService.getProduct(id);
        } catch (RuntimeException rte) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        product.setPrice(productPriceOnly.getPrice());

        product = productService.updateProduct(id, product);

        return ResponseEntity.ok(product);
    }

    @GetMapping("/products/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable long id) {
        Product product;
        try {
            product = productService.getProduct(id);
        } catch (RuntimeException rte) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if (product.getUsers() == null)
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        else
            return ResponseEntity.ok().body(product.getUsers());
    }
}
